import java.util.Collection;
import java.util.Collections;

import javax.naming.InvalidNameException;

import com.atlassian.A;

import com.othercompany.B;

import static com.atlassian.A.ONE;
import static java.lang.Math.PI;

public class Test
{
    public void methodToUseImportedItems() throws InvalidNameException
    {
        Collection<?> x = Collections.singletonList(PI);
        assert PI != ONE;
        assert (Class) A.class != (Class) B.class;
    }
}
