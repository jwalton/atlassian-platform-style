////////////////////////////////////////////////////////////////////////////////
// checkstyle: Checks Java source code for adherence to a set of rules.
// Copyright (C) 2001-2012  Oliver Burn
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
////////////////////////////////////////////////////////////////////////////////

package com.atlassian.platform.style;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.puppycrawl.tools.checkstyle.api.Check;
import com.puppycrawl.tools.checkstyle.api.DetailAST;
import com.puppycrawl.tools.checkstyle.api.FullIdent;
import com.puppycrawl.tools.checkstyle.api.TokenTypes;

/**
 * <p>
 * Checks specifically for the Atlassian Platform import order:
 * </p>
 *
 * <ol>
 * <li>import java.*</li>
 * <li>import javax.*</li>
 * <li>import com.atlassian.*</li>
 * <li>import com.*</li>
 * <li>import org.*</li>
 * <li>import all other imports</li>
 * <li>import static all other imports</li>
 * </ol>
 *
 * <p>with blank lines between each group.</p>
 *
 * <p>Derived from {@link com.puppycrawl.tools.checkstyle.checks.imports.ImportOrderCheck}.</p>
 *
 * @author Bill Schneider
 * @author o_sukhodolsky
 * @author David DIDIER
 * @author Steve McKay
 */
public class ImportOrderCheck extends Check
{
    private static final Pattern[] mGroups = {
            Pattern.compile("^java\\."),
            Pattern.compile("^javax\\."),
            Pattern.compile("^com\\.atlassian\\."),
            Pattern.compile("^com\\."),
            Pattern.compile("^org\\."),
    };

    /** Require imports in group be separated. */
    private static final boolean mSeparated = true;

    /** Last imported group. */
    private CategorisedImport lastImport;
    private int lastImportLine;

    @Override
    public int[] getDefaultTokens()
    {
        return new int[] {TokenTypes.IMPORT, TokenTypes.STATIC_IMPORT};
    }

    @Override
    public void beginTree(DetailAST aRootAST)
    {
        lastImport = null;
    }

    @Override
    public void visitToken(DetailAST aAST)
    {
        FullIdent ident;
        boolean isStatic;
        int groupNumber;

        switch (aAST.getType())
        {
        case TokenTypes.IMPORT:
            ident = FullIdent.createFullIdentBelow(aAST);
            isStatic = false;
            groupNumber = getGroupNumber(ident.getText());
            break;

        case TokenTypes.STATIC_IMPORT:
            ident = FullIdent.createFullIdent(aAST.getFirstChild()
                    .getNextSibling());
            isStatic = true;
            groupNumber = -1;
            break;

        default:
            throw new IllegalArgumentException("Not expecting to visit: " + aAST.getType());
        }

        CategorisedImport thisImport = new CategorisedImport(isStatic, groupNumber, ident.getText());
        int thisImportLine = ident.getLineNo();

        if (lastImport != null)
        {
            if (lastImport.compareTo(thisImport) > 0)
            {
                log(ident.getLineNo(), "import.ordering", thisImport);
            }

            if (mSeparated)
            {
                if (!lastImport.sameGroup(thisImport))
                {
                    if (thisImportLine - lastImportLine < 2)
                    {
                        log(ident.getLineNo(), "import.separation", ident.getText());
                    }
                }
            }
        }

        lastImport = thisImport;
        lastImportLine = thisImportLine;
    }

    /**
     * Finds out what group the specified import belongs to.
     *
     * @param aName the import name to find.
     * @return group number for given import name.
     */
    private int getGroupNumber(String aName)
    {
        int bestIndex = mGroups.length;
        int bestLength = -1;
        int bestPos = 0;

        // find out what group this belongs in
        // loop over mGroups and get index
        for (int i = 0; i < mGroups.length; i++) {
            final Matcher matcher = mGroups[i].matcher(aName);
            while (matcher.find()) {
                final int length = matcher.end() - matcher.start();
                if ((length > bestLength)
                    || ((length == bestLength) && (matcher.start() < bestPos)))
                {
                    bestIndex = i;
                    bestLength = length;
                    bestPos = matcher.start();
                }
            }
        }

        return bestIndex;
    }

    private static class CategorisedImport implements Comparable<CategorisedImport>
    {
        private final boolean isStatic;
        private final int groupNumber;
        private final String importValue;

        public CategorisedImport(boolean isStatic, int groupNumber, String importValue)
        {
            this.isStatic = isStatic;
            this.groupNumber = groupNumber;
            this.importValue = importValue;
        }

        boolean sameGroup(CategorisedImport o)
        {
            if (isStatic && o.isStatic)
            {
                return true;
            }

            if (isStatic != o.isStatic)
            {
                return false;
            }

            return groupNumber == o.groupNumber;
        }

        @Override
        public int compareTo(CategorisedImport o)
        {
            int bc = Boolean.valueOf(isStatic).compareTo(Boolean.valueOf(o.isStatic));
            if (bc != 0)
            {
                return bc;
            }

            int gc = Integer.valueOf(groupNumber).compareTo(Integer.valueOf(o.groupNumber));
            if (gc != 0)
            {
                return gc;
            }

            return importValue.compareTo(o.importValue);
        }

        @Override
        public String toString()
        {
            return (isStatic ? "static " : "") + importValue; // + " (matches group " + groupNumber + ")";
        }
    }
}
